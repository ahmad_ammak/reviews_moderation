__author__ = 'aabukhalil'
import csv
FILE_NAME = "../reviews2.tsv"
file = open(FILE_NAME)
not_rated = 0
all_data = []
accept = 0
reject = 0
is_published_rating_relation = dict()
for record in csv.reader(file, delimiter="\t", quoting=csv.QUOTE_NONE):
    if len(record) != 23 or record[9] in ('0', '90'):
        continue
    is_published = record[17]
    rating = record[9]
    if rating not in is_published_rating_relation:
        is_published_rating_relation[rating] = dict()
        is_published_rating_relation[rating][is_published] = 1
    else:
        if is_published  not in is_published_rating_relation[rating]:
            is_published_rating_relation[rating][is_published] = 1
        else:
            is_published_rating_relation[rating][is_published] += 1
    if record[17] == '1':
        accept += 1
    elif record[17] == '0':
        reject += 1
print(is_published_rating_relation)
is_published_rating_relation2 = is_published_rating_relation.copy()
for rating in is_published_rating_relation2:
    is_published_rating_relation2[rating]['0'] = float(is_published_rating_relation2[rating]['0']) / reject
    is_published_rating_relation2[rating]['1'] = float(is_published_rating_relation2[rating]['1']) / accept
print(is_published_rating_relation2)



# rating classification
# from here

# import csv, random
# from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.linear_model import SGDClassifier
# def getMatrixColumn(matrix, column_idx):
#     return [record[column_idx] for record in matrix]
# FILE_NAME = "../reviews2.tsv"
# file = open(FILE_NAME)
# not_rated = 0
# all_data = []
# accept = 0
# reject = 0
# d =dict()
# for record in csv.reader(file, delimiter="\t", quoting=csv.QUOTE_NONE):
#     if len(record) != 23:
#         continue
#
#     if record[9] == '1' or record[9] == '2':
#         rating = '0'
#     elif record[9] == '4' or record[9] == '5':
#         rating = '1'
#     else:
#
#         not_rated += 1
#         continue
#     if rating not in d:
#         d[rating] = 1
#     else:
#         d[rating] += 1
#     all_data.append((record[7], record[8], record[18], record[19], rating, record[17]))
#     #               title       text       adv          disadv     rating     is_pub
#     if record[17] == '1':
#         accept += 1
#     elif record[17] == '0':
#         reject += 1
#
# print(d)
# print(not_rated)
# random.shuffle(all_data)
# final_data = [(record[0]+"\n"+record[1]+"\n"+record[2]+"\n"+record[3], record[4]) for record in all_data]
# del all_data
# del file
# SPLIT_RATIO = 0.99
# split_idx = int(len(final_data) * SPLIT_RATIO)
# train_set = final_data[:split_idx]
# test_set = final_data[split_idx:]
# print("Split ratio :"+str(SPLIT_RATIO))
# print("Total samples :"+str(len(final_data)))
# print("Training set len :"+str(len(train_set)))
# print("Testing set len :"+str(len(test_set)))
# transformer1 = CountVectorizer(ngram_range=(1, 3))
# transformer1.fit(getMatrixColumn(train_set, 0))
# classifier = SGDClassifier(loss="log", n_jobs=-1)
# classifier.fit(transformer1.transform(getMatrixColumn(train_set, 0)), getMatrixColumn(train_set, 1))
# print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))


# multi rating classification accuracy : 0.636
# binary rate classification accuracy : 0.89 -


# to here