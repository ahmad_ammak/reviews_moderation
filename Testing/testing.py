__author__ = 'aabukhalil'
import csv
import random
import time
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.externals.joblib import dump,load
def getMatrixColumn(matrix, column_idx):
    return [record[column_idx] for record in matrix]

def moderateReview(review_feats, classifier, accept_threshold=0.50, reject_threshold=0.50):
    results = classifier.predict_proba(review_feats)[0]
    class_results = []
    for i in range(len(results)):
        class_results.append((classifier.classes_[i], results[i]))
    class_results = sorted(class_results, key=lambda x: x[1], reverse=True)
    if class_results[0][0] == '0' and class_results[0][1] >= reject_threshold:
        return '0'
    if class_results[0][0] == '1' and class_results[0][1] >= accept_threshold:
        return '1'
    return False


def getPrecandRec(test_set, classifier, transformer, accept_threshold=0.70, reject_threshold=0.60):
    actual_accept = 0
    actual_reject = 0
    accept_predicted = 0
    reject_predicted = 0
    accept_predicted_correct = 0
    reject_precicted_correct = 0
    for test_case in test_set:
        print(test_case[0])
        actual = test_case[1]
        print("actual :"+str(actual))
        predected = moderateReview(transformer.transform([test_case[0]]), classifier,
                                   accept_threshold=accept_threshold, reject_threshold=reject_threshold)
        print("predicted :"+str(predected))

        if str(actual) == '1':
            actual_accept += 1
            if predected:
                accept_predicted += 1
                if str(predected) == '1':
                    accept_predicted_correct += 1
        else:
            actual_reject += 1
            if predected:
                reject_predicted += 1
                if str(predected) == '0':
                    reject_precicted_correct += 1

    result = ""
    result += "Accept Threshold :"+str(accept_threshold)+"\n"
    result += "Reject Threshold :"+str(accept_threshold)+"\n"
    result += "Number of test cases :"+str(actual_reject+actual_accept)+"\n"
    result += "Number of actual published :"+str(actual_accept)+"\n"
    result += str(round(accept_predicted/float(actual_accept), 5))+" of published reviews predicted ("+str(accept_predicted)+")\n"
    result += str(round(accept_predicted_correct/float(accept_predicted), 5))+" of them is correct ("+str(accept_predicted_correct)+")\n"
    result += str(round(accept_predicted-accept_predicted_correct/float(accept_predicted), 5))+" of them is wrong ("+str(accept_predicted-accept_predicted_correct)+")\n"
    result += "Number of not predicted reviews :"+str(actual_accept-accept_predicted)+"\n"
    result += "Number of actual rejected :"+str(actual_reject)+"\n"
    result += str(round(reject_predicted/float(actual_reject), 5))+" of rejected reviews predicted ("+str(reject_predicted)+")\n"
    result += str(round(reject_precicted_correct/float(reject_predicted), 5))+" of them is correct ("+str(reject_precicted_correct)+")\n"
    result += str(round(reject_predicted-reject_precicted_correct/float(reject_predicted), 5))+" of them is wrong ("+str(reject_predicted-reject_precicted_correct)+")\n"
    result += "Number of not predicted reviews :"+str(actual_reject-reject_predicted)+"\n"
    result += "Total number of not predicted reviews :"+str(actual_accept+actual_reject-accept_predicted-reject_predicted)+"\n"
    return result

from nltk.stem import ISRIStemmer, PorterStemmer
import re
def stemWord(word):
    en_pattern  = re.compile("[a-zA-Z]+")
    ar_pattern  = re.compile("[\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]+")
    en_word = en_pattern.search(word)
    ar_word = ar_pattern.search(word)
    if en_word and ar_word:
        return word
    if en_word:
        en_stemmer = PorterStemmer()
        return en_stemmer.stem(word)
    if ar_word:
        ar_stemmer = ISRIStemmer()
        return ar_stemmer.stem(word)

def stemSentence(sentence):
    ret = ""
    for word in sentence.split():
        ret += str(stemWord(word))+" "
    return ret

import re
email_pattern = re.compile(".+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)")
mobilenumber_pattern = re.compile(".*([+0-9]{1,3})*([0-9]{9,11}).*")
username_pattern = re.compile(".*[a-zA-Z0-9_-]*[_\-@]+[a-zA-Z0-9_-]{2,}.*")
url_pattern = re.compile(".*(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?.*")
def contains_email(sentence):
    return email_pattern.findall(sentence)

def contains_mobilenumber(sentence):

    return mobilenumber_pattern.findall(sentence)

def contains_username(sentence):
    return username_pattern.findall(sentence)

def contains_url(sentence):
    return url_pattern.findall(sentence)


def add_manual_features(sentence):
    ret = sentence+"\n"
    if contains_url(sentence):
        ret += "containsurl "
    if contains_username(sentence):
        ret += "containsusername "
    if contains_mobilenumber(sentence):
        ret += "containsmobilenumber "
    if contains_email(sentence):
        ret += "containsemail "
    return ret


file = open("../reviews2.tsv")
all_data = []
accept = 0
reject = 0
for record in csv.reader(file, delimiter="\t", quoting=csv.QUOTE_NONE):
    if len(record) != 23:
        continue
    text = record[7]+"\n"+record[8]+"\n"+record[18]+"\n"+record[19]
    all_data.append((record[7], record[8], record[18], record[19], record[9], record[17]))
    #               title       text       adv          disadv     rating     is_pub
    if record[17] == '1':
        accept += 1
    elif record[17] == '0':
        reject += 1
print(accept/float(len(all_data)))
print(reject/float(len(all_data)))
random.shuffle(all_data)
final_data = [(record[0]+"\n"+record[1]+"\n"+record[2]+"\n"+record[3],record[5]) for record in all_data]
SPLIT_RATIO = 0.99
split_idx = int(len(final_data) * SPLIT_RATIO)
train_set = final_data[:split_idx]
test_set = final_data[split_idx:]
print("Split ratio :"+str(SPLIT_RATIO))
print("Total samples :"+str(len(final_data)))
print("Training set len :"+str(len(train_set)))
print("Testing set len :"+str(len(test_set)))
LOAD = True
SAVE = False
USE_PAR = False
MANUAL_TEST = False
CLF_NAME = "sgd_log_clf_099_ds2"
TRF_NAME = "transformer_binary_099_dst"
start_time = time.time()
if not LOAD:
    transformer1 = CountVectorizer(ngram_range=(1, 3))
    transformer1.fit(getMatrixColumn(train_set, 0))
    print(transformer1.get_feature_names())
    exit()
    if SAVE:
        dump(transformer1, TRF_NAME)
    classifier = SGDClassifier(loss="log", n_jobs=-1)
    classifier.fit(transformer1.transform(getMatrixColumn(train_set, 0)), getMatrixColumn(train_set, 1))
    if SAVE:
        dump(classifier, CLF_NAME)
else:
    transformer1 = load(TRF_NAME)
    classifier = load(CLF_NAME)
print(str(time.time()-start_time)+" sec")
if USE_PAR:
    print(getPrecandRec(test_set, classifier, transformer1, accept_threshold=0.70, reject_threshold=0.60))
else:
    print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))
while True and MANUAL_TEST:
    s = input("Enter review : ")
    print(moderateReview(transformer1.transform([s]),classifier))
#     print(classifier.predict_proba(transformer1.transform([s])))
# print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))

