__author__ = 'aabukhalil'
import csv
import random
import time
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.externals.joblib import dump,load
from sklearn.naive_bayes import MultinomialNB
def getMatrixColumn(matrix, column_idx):
    return [record[column_idx] for record in matrix]

def moderateReview(review_feats, classifier, accept_threshold=0.50, reject_threshold=0.50):
    results = classifier.predict_proba(review_feats)[0]
    class_results = []
    for i in range(len(results)):
        class_results.append((classifier.classes_[i], results[i]))
    class_results = sorted(class_results, key=lambda x: x[1], reverse=True)
    if class_results[0][0] == '0' and class_results[0][1] >= reject_threshold:
        return '0'
    if class_results[0][0] == '1' and class_results[0][1] >= accept_threshold:
        return '1'
    return False


def getPrecandRec(test_set, classifier, transformer, accept_threshold=0.70, reject_threshold=0.60):
    actual_accept = 0
    actual_reject = 0
    accept_predicted = 0
    reject_predicted = 0
    accept_predicted_correct = 0
    reject_precicted_correct = 0
    for test_case in test_set:
        print(test_case[0])
        actual = test_case[1]
        print("actual :"+str(actual))
        predected = moderateReview(transformer.transform([test_case[0]]), classifier,
                                   accept_threshold=accept_threshold, reject_threshold=reject_threshold)
        print("predicted :"+str(predected))

        if str(actual) == '1':
            actual_accept += 1
            if predected:
                accept_predicted += 1
                if str(predected) == '1':
                    accept_predicted_correct += 1
        else:
            actual_reject += 1
            if predected:
                reject_predicted += 1
                if str(predected) == '0':
                    reject_precicted_correct += 1

    result = ""
    result += "Accept Threshold :"+str(accept_threshold)+"\n"
    result += "Reject Threshold :"+str(reject_threshold)+"\n"
    result += "Number of test cases :"+str(actual_reject+actual_accept)+"\n"
    result += "Number of actual published :"+str(actual_accept)+"\n"
    result += str(round(accept_predicted/float(actual_accept), 5))+" of published reviews predicted ("+str(accept_predicted)+")\n"
    result += str(round(accept_predicted_correct/float(accept_predicted), 5))+" of them is correct ("+str(accept_predicted_correct)+")\n"
    result += str(round(accept_predicted-accept_predicted_correct/float(accept_predicted), 5))+" of them is wrong ("+str(accept_predicted-accept_predicted_correct)+")\n"
    result += "Number of not predicted reviews :"+str(actual_accept-accept_predicted)+"\n"
    result += "Number of actual rejected :"+str(actual_reject)+"\n"
    result += str(round(reject_predicted/float(actual_reject), 5))+" of rejected reviews predicted ("+str(reject_predicted)+")\n"
    result += str(round(reject_precicted_correct/float(reject_predicted), 5))+" of them is correct ("+str(reject_precicted_correct)+")\n"
    result += str(round(reject_predicted-reject_precicted_correct/float(reject_predicted), 5))+" of them is wrong ("+str(reject_predicted-reject_precicted_correct)+")\n"
    result += "Number of not predicted reviews :"+str(actual_reject-reject_predicted)+"\n"
    result += "Total number of not predicted reviews :"+str(actual_accept+actual_reject-accept_predicted-reject_predicted)+"\n"
    return result


from nltk.stem import ISRIStemmer, PorterStemmer
import re
def stemWord(word):
    en_pattern  = re.compile("[a-zA-Z]+")
    ar_pattern  = re.compile("[\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]+")
    en_word = en_pattern.search(word)
    ar_word = ar_pattern.search(word)
    if en_word and ar_word:
        return word
    if en_word:
        en_stemmer = PorterStemmer()
        return en_stemmer.stem(word)
    if ar_word:
        ar_stemmer = ISRIStemmer()
        return ar_stemmer.stem(word)

def stemSentence(sentence):
    ret = ""
    for word in sentence.split():
        ret += str(stemWord(word))+" "
    return ret

import re
email_pattern = re.compile(".+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)")
mobilenumber_pattern = re.compile(".*([+0-9]{1,3})*([0-9]{9,11}).*")
username_pattern = re.compile(".*[a-zA-Z0-9_-]*[_\-@]+[a-zA-Z0-9_-]{2,}.*")
url_pattern = re.compile(".*(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?.*")


def contains_email(sentence):
    return email_pattern.findall(sentence)


def contains_mobilenumber(sentence):

    return mobilenumber_pattern.findall(sentence)


def contains_username(sentence):
    return username_pattern.findall(sentence)


def contains_url(sentence):
    return url_pattern.findall(sentence)


def add_manual_features(sentence, rating=None, recommended=None):
    ret = sentence+"\n"
    if contains_url(sentence):
        ret += "containsurl "
    if contains_username(sentence):
        ret += "containsusername "
    if contains_mobilenumber(sentence):
        ret += "containsmobilenumber "
    if contains_email(sentence):
        ret += "containsemail "
    if rating is not None and rating not in ('0', '90'):
        ret += "rating"+str(rating)+" "
    if recommended is not None:
        ret += "recommended"+recommended+"recommended"
    return ret

def remove_n_sequent_chars(sentence, n=2):
    if len(sentence) < n:
        return sentence
    sentences = []
    for i in range(1, n+1):
        addition = ""
        for j in range(i):
            addition += " "
        sentences.append(addition+sentence)
    ret = ""
    for char_idx in range(len(sentence)):
        add = False
        for added in sentences:
            if added[char_idx] != sentence[char_idx]:
                add = True
        if add:
            ret += sentence[char_idx]
    return ret


file = open("../reviews2.tsv")
all_data = []
accept = 0
reject = 0
c = 0
for record in csv.reader(file, delimiter="\t", quoting=csv.QUOTE_NONE):
    if len(record) != 23:
        continue

    #               title       text       adv          disadv     rating     is_pub
    text = record[7]+"\n"+record[8]+"\n"+record[18]+"\n"+record[19]
    text = remove_n_sequent_chars(text)
    text = stemSentence(text)
    rating = record[9]
    recommended = record[20]
    text = add_manual_features(text, rating, recommended)
    all_data.append((text, record[17]))
    if record[17] == '1':
        accept += 1
    elif record[17] == '0':
        reject += 1
    if c%1000 == 0:
        print(c)
    # if c == 1000:
    #     break
    c+=1
print(accept/float(len(all_data)))
print(reject/float(len(all_data)))
final_data = [(record[0],record[1]) for record in all_data]
print("Log -----------")

print("naive bayes \n\n\n\n")
for i in range(3):
    random.shuffle(final_data)
    SPLIT_RATIO = 0.75
    split_idx = int(len(final_data) * SPLIT_RATIO)
    train_set = final_data[:split_idx]
    test_set = final_data[split_idx:]
    print("Split ratio :"+str(SPLIT_RATIO))
    print("Total samples :"+str(len(final_data)))
    print("Training set len :"+str(len(train_set)))
    print("Testing set len :"+str(len(test_set)))
    LOAD = False
    SAVE = False
    USE_PAR = False
    MANUAL_TEST = False
    CLF_NAME = "sgd_log_clf_099_ds2_newfeat_and_rating"
    TRF_NAME = "transformer_binary_099_dst_newfeat_and_rating"
    start_time = time.time()
    if not LOAD:
        transformer1 = CountVectorizer(ngram_range=(1, 3))
        transformer1.fit(getMatrixColumn(train_set, 0))
        if SAVE:
            dump(transformer1, TRF_NAME)
        classifier = MultinomialNB()
        classifier.fit(transformer1.transform(getMatrixColumn(train_set, 0)), getMatrixColumn(train_set, 1))
        if SAVE:
            dump(classifier, CLF_NAME)
    else:
        transformer1 = load(TRF_NAME)
        classifier = load(CLF_NAME)
    print(str(time.time()-start_time)+" sec")
    if USE_PAR:
        print(getPrecandRec(test_set, classifier, transformer1, accept_threshold=0.70, reject_threshold=0.60))
    else:
        print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))
    while True and MANUAL_TEST:
        s = add_manual_features(stemSentence(remove_n_sequent_chars(input("Enter review : "))))
        print(moderateReview(transformer1.transform([s]),classifier))
    #     print(classifier.predict_proba(transformer1.transform([s])))
    # print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))


print("sgd log\n\n\n\n")
for i in range(3):
    random.shuffle(final_data)
    SPLIT_RATIO = 0.75
    split_idx = int(len(final_data) * SPLIT_RATIO)
    train_set = final_data[:split_idx]
    test_set = final_data[split_idx:]
    print("Split ratio :"+str(SPLIT_RATIO))
    print("Total samples :"+str(len(final_data)))
    print("Training set len :"+str(len(train_set)))
    print("Testing set len :"+str(len(test_set)))
    LOAD = False
    SAVE = False
    USE_PAR = False
    MANUAL_TEST = False
    CLF_NAME = "sgd_log_clf_099_ds2_newfeat_and_rating"
    TRF_NAME = "transformer_binary_099_dst_newfeat_and_rating"
    start_time = time.time()
    if not LOAD:
        transformer1 = CountVectorizer(ngram_range=(1, 3))
        transformer1.fit(getMatrixColumn(train_set, 0))
        if SAVE:
            dump(transformer1, TRF_NAME)
        classifier = SGDClassifier(loss="log", n_jobs=-1)
        classifier.fit(transformer1.transform(getMatrixColumn(train_set, 0)), getMatrixColumn(train_set, 1))
        if SAVE:
            dump(classifier, CLF_NAME)
    else:
        transformer1 = load(TRF_NAME)
        classifier = load(CLF_NAME)
    print(str(time.time()-start_time)+" sec")
    if USE_PAR:
        print(getPrecandRec(test_set, classifier, transformer1, accept_threshold=0.70, reject_threshold=0.60))
    else:
        print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))
    while True and MANUAL_TEST:
        s = add_manual_features(stemSentence(remove_n_sequent_chars(input("Enter review : "))))
        print(moderateReview(transformer1.transform([s]),classifier))
    #     print(classifier.predict_proba(transformer1.transform([s])))
    # print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))


print("sgd svm \n\n\n\n")
for i in range(3):
    random.shuffle(final_data)
    SPLIT_RATIO = 0.75
    split_idx = int(len(final_data) * SPLIT_RATIO)
    train_set = final_data[:split_idx]
    test_set = final_data[split_idx:]
    print("Split ratio :"+str(SPLIT_RATIO))
    print("Total samples :"+str(len(final_data)))
    print("Training set len :"+str(len(train_set)))
    print("Testing set len :"+str(len(test_set)))
    LOAD = False
    SAVE = False
    USE_PAR = False
    MANUAL_TEST = False
    CLF_NAME = "sgd_log_clf_099_ds2_newfeat_and_rating"
    TRF_NAME = "transformer_binary_099_dst_newfeat_and_rating"
    start_time = time.time()
    if not LOAD:
        transformer1 = CountVectorizer(ngram_range=(1, 3))
        transformer1.fit(getMatrixColumn(train_set, 0))
        if SAVE:
            dump(transformer1, TRF_NAME)
        classifier = SGDClassifier(n_jobs=-1)
        classifier.fit(transformer1.transform(getMatrixColumn(train_set, 0)), getMatrixColumn(train_set, 1))
        if SAVE:
            dump(classifier, CLF_NAME)
    else:
        transformer1 = load(TRF_NAME)
        classifier = load(CLF_NAME)
    print(str(time.time()-start_time)+" sec")
    if USE_PAR:
        print(getPrecandRec(test_set, classifier, transformer1, accept_threshold=0.70, reject_threshold=0.60))
    else:
        print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))
    while True and MANUAL_TEST:
        s = add_manual_features(stemSentence(remove_n_sequent_chars(input("Enter review : "))))
        print(moderateReview(transformer1.transform([s]),classifier))
    #     print(classifier.predict_proba(transformer1.transform([s])))
    # print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))
