__author__ = 'ahmad'
from nltk.stem import ISRIStemmer, PorterStemmer
import re
def stemWord(word):
    en_pattern = re.compile("[a-zA-Z]+")
    ar_pattern = re.compile("[\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]+")
    en_word = en_pattern.search(word)
    ar_word = ar_pattern.search(word)
    if en_word and ar_word:
        return word
    if en_word:
        en_stemmer = PorterStemmer()
        return en_stemmer.stem(word)
    if ar_word:
        ar_stemmer = ISRIStemmer()
        return ar_stemmer.stem(word)

def stemSentence(sentence):
    ret = ""
    for word in sentence.split():
        ret += str(stemWord(word))+" "
    return ret

import re
email_pattern = re.compile(".+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)")
mobilenumber_pattern = re.compile(".*([+0-9]{1,3})*([0-9]{9,11}).*")
username_pattern = re.compile(".*[a-zA-Z0-9_-]*[_\-@]+[a-zA-Z0-9_-]{2,}.*")
url_pattern = re.compile(".*(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?.*")
def contains_email(sentence):
    return email_pattern.findall(sentence)

def contains_mobilenumber(sentence):

    return mobilenumber_pattern.findall(sentence)

def contains_username(sentence):
    return username_pattern.findall(sentence)

def contains_url(sentence):
    return url_pattern.findall(sentence)


def add_manual_features(sentence):
    ret = sentence+"\n"
    if contains_url(sentence):
        ret += "containsurl "
    if contains_username(sentence):
        ret += "containsusername "
    if contains_mobilenumber(sentence):
        ret += "containsmobilenumber "
    if contains_email(sentence):
        ret += "containsemail "
    return ret

def remove_n_sequent_chars(sentence, n=2):
    if len(sentence) < n:
        return sentence
    sentences = []
    for i in range(1, n+1):
        addition = ""
        for j in range(i):
            addition += " "
        sentences.append(addition+sentence)
    ret = ""
    for char_idx in range(len(sentence)):
        add = False
        for added in sentences:
            if added[char_idx] != sentence[char_idx]:
                add = True
        if add:
            ret += sentence[char_idx]
    return ret