__author__ = 'aabukhalil'
from nltk.stem import ISRIStemmer, PorterStemmer
import re


class Stemmer:
    __en_pattern = re.compile("[a-zA-Z]+")
    __ar_pattern = re.compile("[\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]+")
    __en_stemmer = PorterStemmer()
    __ar_stemmer = ISRIStemmer()

    @staticmethod
    def stemWord(word):
        en_word = Stemmer.__en_pattern.search(word)
        ar_word = Stemmer.__ar_pattern.search(word)
        if en_word and ar_word:
            return word
        if en_word:
            return Stemmer.__en_stemmer.stem(word)
        if ar_word:
            return Stemmer.__ar_stemmer.stem(word)
        return word

    @staticmethod
    def stemSentence(sentence):
        ret = ""
        for word in sentence.split():
            ret += str(Stemmer.stemWord(word))+" "
        return ret

    @staticmethod
    def remove_n_sequent_chars(sentence, n=2):
        if len(sentence) < n:
            return sentence
        sentences = []
        for i in range(1, n+1):
            addition = ""
            for j in range(i):
                addition += " "
            sentences.append(addition+sentence)
        ret = ""
        for char_idx in range(len(sentence)):
            add = False
            for added in sentences:
                if added[char_idx] != sentence[char_idx]:
                    add = True
            if add:
                ret += sentence[char_idx]
        return ret
