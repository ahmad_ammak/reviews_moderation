__author__ = 'aabukhalil'
import redis, pickle


class RedisTransformerClassifierLoaderAndStorer:
    def __init__(self, config):
        super().__init__()
        self.cofig = config
        self.redis_client = redis.StrictRedis(host=config.get("REDIS", "host"),
                                              port=config.getint("REDIS", "port"),
                                              db=config.getint("REDIS", "database"))

    def is_redis_contains_classifier_and_transformer(self):
        transformer_key = self.cofig.get("REDIS", "transformer_key")
        classifier_key = self.cofig.get("REDIS", "classifier_key")
        if not (self.redis_client.exists(transformer_key) and self.redis_client.exists(classifier_key)):
            self.redis_client.delete(self.cofig.get("REDIS", "last_data_id_key"))
        return self.redis_client.exists(transformer_key) and self.redis_client.exists(classifier_key)

    def is_redis_contains_annotated_data(self):
        annotated_data_key = self.cofig.get("REDIS", "annotated_data")
        return self.redis_client.exists(annotated_data_key)


    def load_classifier_and_transformer(self):
        if not self.is_redis_contains_classifier_and_transformer():
            return None
        transformer_key = self.cofig.get("REDIS", "transformer_key")
        classifier_key = self.cofig.get("REDIS", "classifier_key")
        pickeled_classifier = self.redis_client.get(classifier_key)
        pickeled_transformer = self.redis_client.get(transformer_key)
        classifier = pickle.loads(pickeled_classifier)
        transformer = pickle.loads(pickeled_transformer)
        return classifier, transformer

    def store_classifier_and_transformer(self, classifier, transformer):
        transformer_key = self.cofig.get("REDIS", "transformer_key")
        classifier_key = self.cofig.get("REDIS", "classifier_key")
        pickeled_classifier = pickle.dumps(classifier)
        pickeled_transformer = pickle.dumps(transformer)
        self.redis_client.set(transformer_key, pickeled_transformer)
        self.redis_client.set(classifier_key, pickeled_classifier)
        return True

    def load_annotated_data(self):
        if self.is_redis_contains_annotated_data():
            return None
        annotated_data_key = self.cofig.get("REDIS", "annotated_data")
        pickeled_annotated_data = self.redis_client.get(annotated_data_key)
        annotated_data = pickle.loads(pickeled_annotated_data)
        return annotated_data

    def store_annotated_data(self, annotated_data):
        annotated_data_key = self.cofig.get("REDIS", "annotated_data")
        pickeled_annotated_data = pickle.dumps(annotated_data)
        self.redis_client.set(annotated_data_key, pickeled_annotated_data)
        return True

    def get_last_trained_data_id(self):
        last_id_key = self.cofig.get("REDIS", "last_data_id_key")
        pickeled_last_id = self.redis_client.get(last_id_key)
        last_id = pickle.loads(pickeled_last_id)
        return last_id

    def set_last_trained_data_id(self, last_id):
        last_id_key = self.cofig.get("REDIS", "last_data_id_key")
        pickeled_last_id = pickle.dumps(last_id)
        self.redis_client.set(last_id_key, pickeled_last_id)
        return True

    def is_redis_contains_last_trained_data_id(self):
        last_id_key = self.cofig.get("REDIS", "last_data_id_key")
        return self.redis_client.exists(last_id_key)

