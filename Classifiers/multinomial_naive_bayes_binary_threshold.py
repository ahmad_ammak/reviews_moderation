__author__ = 'aabukhalil'
import csv
import random
import time
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
def getMatrixColumn(matrix, column_idx):
    return [record[column_idx] for record in matrix]

def moderateReview(review_feats,classifier,threshold=0.50):
    results = classifier.predict_proba(review_feats)[0]
    class_results = []
    for i in range(len(results)):
        class_results.append((classifier.classes_[i],results[i]))
    class_results = sorted(class_results,key=lambda x:x[1],reverse=True)
    if class_results[0][1] >= threshold:
        return class_results[0][0]
    return False

def getPrecandRec(test_set,classifier,transformer):
    prec = 0
    reca = 0
    for test_case in test_set:
        print(test_case[0])
        actual = test_case[1]
        print("actual :"+str(actual))
        predected = moderateReview(transformer1.transform([test_case[0]]),classifier,threshold=.60)
        print("predected :"+str(predected))
        if predected == False:
            prec += 1
        else:
            if actual == predected:
                prec += 1
                reca += 1

    return prec/float(len(test_set)),reca/float(len(test_set))


file = open("../reviews.tsv")
all_data = []
accept = 0
reject = 0
for record in csv.reader(file, delimiter="\t", quoting=csv.QUOTE_NONE):
    if len(record) != 23:
        continue
    all_data.append((record[7], record[8], record[18], record[19], record[9], record[17]))
    #               title       text       adv          disadv     rating     is_pub
    if record[17] == '1':
        accept += 1
    elif record[17] == '0':
        reject += 1
print(accept/float(len(all_data)))
print(reject/float(len(all_data)))
random.shuffle(all_data)
final_data = [(record[0]+"\n"+record[1]+"\n"+record[2]+"\n"+record[3],record[5]) for record in all_data]
del all_data
del file
SPLIT_RATIO = 0.99
split_idx = int(len(final_data) * SPLIT_RATIO)
train_set = final_data[:split_idx]
test_set = final_data[split_idx:]
print("Split ratio :"+str(SPLIT_RATIO))
print("Total samples :"+str(len(final_data)))
print("Training set len :"+str(len(train_set)))
print("Testing set len :"+str(len(test_set)))
del final_data
start_time = time.time()
transformer1 = CountVectorizer(ngram_range=(1, 3))
transformer1.fit(getMatrixColumn(train_set, 0))
classifier = MultinomialNB()
classifier.fit(transformer1.transform(getMatrixColumn(train_set, 0)), getMatrixColumn(train_set, 1))
print(str(time.time()-start_time)+" sec")
print(getPrecandRec(test_set,classifier,transformer1))
exit()
while True:
    s = input("Enter review : ")
    print(moderateReview(transformer1.transform([s]),classifier))
#     print(classifier.predict_proba(transformer1.transform([s])))
# print(classifier.score(transformer1.transform(getMatrixColumn(test_set, 0)), getMatrixColumn(test_set, 1)))
