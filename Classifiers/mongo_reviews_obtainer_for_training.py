__author__ = 'aabukhalil'
from pymongo import MongoClient
from Classifiers.redis_transformer_classifier_loaderstorer import RedisTransformerClassifierLoaderAndStorer

class TrainingReviewsObtainer:
    __required_fields = {'title': 1, 'text': 1, 'item_rating': 1, 'advantages': 1, 'disadvantages': 1, 'recommended': 1,
                         'is_published': 1}

    def __init__(self, config):
        super().__init__()
        client = MongoClient(config.get("MONGODB", "host"), config.getint("MONGODB", "port"))
        db = client.get_database(config.get("MONGODB", "database"))
        TrainingReviewsObtainer.collection_name = "all_reviews"
        TrainingReviewsObtainer.collection = db.get_collection(TrainingReviewsObtainer.collection_name)
        self.redis_client = RedisTransformerClassifierLoaderAndStorer(config)
        self.config = config

    def get_last_id(self):
        review = [i for i in TrainingReviewsObtainer.collection.find().sort("_id", -1).limit(1)]
        review = review[0]
        return review["_id"]

    def get_all_reviews_for_training(self):
        all_reviews = []
        for review in TrainingReviewsObtainer.collection.find({}, TrainingReviewsObtainer.__required_fields):
            for required_field in TrainingReviewsObtainer.__required_fields:
                if required_field not in review:
                    review[required_field] = ""
            all_reviews.append(review)
        return all_reviews

    def get_all_reviews_after_last_train(self):
        if not self.redis_client.is_redis_contains_last_trained_data_id():
            all_reviews = self.get_all_reviews_for_training()
            last_id = self.get_last_id()
            self.redis_client.set_last_trained_data_id(last_id)
        else:
            all_reviews = []
            last_id = self.redis_client.get_last_trained_data_id()
            for review in TrainingReviewsObtainer.collection.find({"_id": {"$gt": last_id}}, TrainingReviewsObtainer.__required_fields):
                for required_field in TrainingReviewsObtainer.__required_fields:
                    if required_field not in review:
                        review[required_field] = ""
                all_reviews.append(review)
        return all_reviews

    def is_there_is_new_reviews_more_than_threshold(self, threshold):
        if not self.redis_client.is_redis_contains_last_trained_data_id():
            return True
        last_id = self.redis_client.get_last_trained_data_id()
        new_review_num = TrainingReviewsObtainer.collection.find({"_id": {"$gt": last_id}}, TrainingReviewsObtainer.__required_fields).count()
        if new_review_num > threshold:
            return True, {"Number of new data": str(new_review_num)}
        else:
            return False, {"Number of new data": str(new_review_num)}

    def update_redis_last_key(self):
        self.redis_client.set_last_trained_data_id(self.get_last_id())
        return True
