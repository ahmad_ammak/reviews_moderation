__author__ = 'aabukhalil'
from RESTfulFlaskApi.bootstrap import  *
from sklearn.feature_extraction.text import CountVectorizer, HashingVectorizer
from sklearn.linear_model import SGDClassifier
from Classifiers.stemmer import Stemmer
from Classifiers.custom_features_extractor import CustomFeaturesExtractor
from Classifiers.redis_transformer_classifier_loaderstorer import RedisTransformerClassifierLoaderAndStorer
from Classifiers.mongo_reviews_obtainer_for_training import TrainingReviewsObtainer
import random
import threading

class ReviewClassifier:
    def __init__(self, config):
        super().__init__()
        self.config = config
        self.ready = False
        self.classifier = None
        self.transformer = None
        self.training_data_obtainter = TrainingReviewsObtainer(config)
        self.lock_me = threading.Lock()

    @staticmethod
    def __get_matrix_column(matrix, column_idx):
        return [record[column_idx] for record in matrix]

    def clean_and_add_features_to_review(self, review):
        text = review['title'] + " " + review['text'] + " " + \
               review['advantages'] + " " + review['disadvantages']
        text = Stemmer.remove_n_sequent_chars(text)
        text = Stemmer.stemSentence(text)
        rating = review['item_rating']
        recommended = review['recommended']
        text = CustomFeaturesExtractor.add_manual_features(text, rating, recommended)
        return text

    def extract_feats_from_review(self, review):
        feats = self.transformer.transform([review])
        return feats

    def train_new_classifier_and_transformer(self, save_to_redis=True):
        transformer = HashingVectorizer(ngram_range=(1, 3), non_negative=True)
        classifier = SGDClassifier(loss="log")
        print("obtaining reviews")
        logger.warn("obtaining reviews")
        data = self.training_data_obtainter.get_all_reviews_for_training()
        random.shuffle(data)
        print("obtained reviews")
        logger.warn("obtained reviews")
        annotated_data = []
        for datum in data:
            annotated_data.append((self.clean_and_add_features_to_review(datum), datum["is_published"]))
        transformer.fit(ReviewClassifier.__get_matrix_column(annotated_data, 0))
        transformed_data = transformer.transform(ReviewClassifier.__get_matrix_column(annotated_data, 0))
        classifier.fit(transformed_data, ReviewClassifier.__get_matrix_column(annotated_data, 1))
        self.lock_me.acquire()
        self.classifier = classifier
        self.transformer = transformer
        self.lock_me.release()
        if save_to_redis:
            redis_storer = RedisTransformerClassifierLoaderAndStorer(self.config)
            redis_storer.store_classifier_and_transformer(self.classifier, self.transformer)
            self.training_data_obtainter.update_redis_last_key()
        self.ready = True
        return True

    def update_redis_models_with_new_data(self):
        redis_loader = RedisTransformerClassifierLoaderAndStorer(self.config)
        if not redis_loader.is_redis_contains_classifier_and_transformer():
            return self.train_new_classifier_and_transformer()
        if not self.training_data_obtainter.is_there_is_new_reviews_more_than_threshold(0)[0]:
            return self.load_classifier_from_redis()
        classifier, transformer = redis_loader.load_classifier_and_transformer()
        data = self.training_data_obtainter.get_all_reviews_after_last_train()
        random.shuffle(data)
        new_data = []
        for datum in data:
            new_data.append((self.clean_and_add_features_to_review(datum), datum["is_published"]))
        transformer.partial_fit(ReviewClassifier.__get_matrix_column(new_data, 0))
        transformed_data = transformer.transform(ReviewClassifier.__get_matrix_column(new_data, 0))
        classifier.partial_fit(transformed_data, ReviewClassifier.__get_matrix_column(new_data, 1))
        self.classifier = classifier
        self.transformer = transformer
        redis_storer = RedisTransformerClassifierLoaderAndStorer(self.config)
        redis_storer.store_classifier_and_transformer(classifier=self.classifier, transformer=self.transformer)
        self.training_data_obtainter.update_redis_last_key()
        self.ready = True
        return True

    def load_classifier_from_redis(self):
        redis_loader = RedisTransformerClassifierLoaderAndStorer(self.config)
        if redis_loader.is_redis_contains_classifier_and_transformer():
            self.classifier, self.transformer = redis_loader.load_classifier_and_transformer()
            self.ready = True
            return True
        else:
            return False

    def load_classifier_and_transformer(self):
        self.load_classifier_from_redis()
        if not self.ready:
            self.update_redis_models_with_new_data()
        return self.ready

    def classify(self, review, accept_threshold=0.50, reject_threshold=0.50):
        review_text = self.clean_and_add_features_to_review(review)
        review_feats = self.extract_feats_from_review(review_text)
        results = self.classifier.predict_proba(review_feats)[0]
        class_results = []
        for i in range(len(results)):
            class_results.append((self.classifier.classes_[i], results[i]))
        class_results = sorted(class_results, key=lambda x: x[1], reverse=True)
        if class_results[0][0] == '0' and class_results[0][1] >= reject_threshold:
            return 'rejected', class_results[0][1]
        if class_results[0][0] == '1' and class_results[0][1] >= accept_threshold:
            return 'accepted', class_results[0][1]
        return False

    def is_there_is_new_reviews_more_than_threshold(self, threshold):
        return self.training_data_obtainter.is_there_is_new_reviews_more_than_threshold(threshold)
