__author__ = 'aabukhalil'
import re


class CustomFeaturesExtractor:
    __email_pattern = re.compile(".+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)")
    __mobilenumber_pattern = re.compile(".*([+0-9]{1,3})*([0-9]{9,11}).*")
    __username_pattern = re.compile(".*[a-zA-Z0-9_-]*[_\-@]+[a-zA-Z0-9_-]{2,}.*")
    __url_pattern = re.compile(".*(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?.*")

    @staticmethod
    def __contains_email(sentence):
        return CustomFeaturesExtractor.__email_pattern.findall(sentence)

    @staticmethod
    def __contains_mobilenumber(sentence):
        return CustomFeaturesExtractor.__mobilenumber_pattern.findall(sentence)

    @staticmethod
    def __contains_username(sentence):
        return CustomFeaturesExtractor.__username_pattern.findall(sentence)

    @staticmethod
    def __contains_url(sentence):
        return CustomFeaturesExtractor.__url_pattern.findall(sentence)

    @staticmethod
    def add_manual_features(sentence, rating=None, recommended=None):
        ret = sentence+" "
        if CustomFeaturesExtractor.__contains_url(sentence):
            ret += "containsurl "
        if CustomFeaturesExtractor.__contains_username(sentence):
            ret += "containsusername "
        if CustomFeaturesExtractor.__contains_mobilenumber(sentence):
            ret += "containsmobilenumber "
        if CustomFeaturesExtractor.__contains_email(sentence):
            ret += "containsemail "
        if rating is not None and rating not in ('0', '90') and rating.strip() != "":
            ret += "rating"+str(rating)+" "
        if recommended is not None and recommended.strip() != "":
            ret += "recommended"+recommended+"recommended"
        return ret
