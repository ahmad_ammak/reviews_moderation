__author__ = 'ahmad'
file1 = open("/home/ahmad/Downloads/Documents/Annotated dataset/trainingandtestdata/training.1600000.processed.noemoticon.csv",encoding='iso-8859-1')
file2 = open("/home/ahmad/Downloads/Documents/Annotated dataset/trainingandtestdata/testdata.manual.2009.06.14.csv")
from os import listdir
pos_files = listdir("/home/ahmad/Downloads/Documents/Annotated dataset/aclImdb/train/pos")
neg_files = listdir("/home/ahmad/Downloads/Documents/Annotated dataset/aclImdb/train/neg")
import csv
import re
twitter_handler = re.compile(".*(@[a-zA-Z_\-]+).*")
file3 = open("/home/ahmad/Downloads/Documents/Annotated dataset/trainingandtestdata/alldata.tsv","w")
writer = csv.writer(file3, quoting=csv.QUOTE_ALL)
reader = csv.reader(file1)
for row in reader:
    sentiment = row[0]
    if sentiment == "4":
        sentiment = "1"
    tweet = row[5]
    if twitter_handler.match(tweet):
        tweet = tweet.replace(twitter_handler.match(tweet).group(1), " ")
    writer.writerow((sentiment, tweet))
reader = csv.reader(file2)
for row in reader:
    sentiment = row[0]
    if sentiment == "4":
        sentiment = "1"
    tweet = row[5]
    if twitter_handler.match(tweet):
        tweet = tweet.replace(twitter_handler.match(tweet).group(1), " ")
    writer.writerow((sentiment, tweet))
for file in pos_files:
    tmp_file = open("/home/ahmad/Downloads/Documents/Annotated dataset/aclImdb/train/pos/"+file)
    content = tmp_file.read()
    writer.writerow(("1", content))
    tmp_file.close()
for file in neg_files:
    tmp_file = open("/home/ahmad/Downloads/Documents/Annotated dataset/aclImdb/train/neg/"+file)
    content = tmp_file.read()
    writer.writerow(("0", content))
    tmp_file.close()
file3.close()
