__author__ = 'aabukhalil'
from pymongo import MongoClient
import csv
from dateutil import parser
MONGO_HOST = "localhost"
MONGO_PORT = 27017
DATABASE_NAME = "reviews"
client = MongoClient(host=MONGO_HOST, port=MONGO_PORT)
db = client.get_database(DATABASE_NAME)
all_reviews = db.get_collection("all_reviews")
buyers = dict()
BUYERS_FILE_NAME = "../mydbuyer.tsv"
BRANDS_FILE_NAME = "../myditem.tsv"
REVIEWS_FILE_NAME = "../reviews2.tsv"
have_more_than_one_country = 0
file = open(BUYERS_FILE_NAME)
reader = csv.reader(file, delimiter="\t", quoting=csv.QUOTE_NONE)
for record in reader:
    if len(record) != 2:
        continue
    if record[0] in buyers:
        have_more_than_one_country += 1
    buyers[record[0]] = record[1]
print("have_more_than_one_country : "+str(have_more_than_one_country))
brands = dict()
file = open(BRANDS_FILE_NAME)
reader = csv.reader(file, delimiter="\t", quoting=csv.QUOTE_NONE)
for record in reader:
    if len(record) != 3:
        continue
    brands[record[0]] = (record[1], record[2])
FIELD_NAMES = ["id_item_review", "id_item", "id_author", "id_language", "author_name", "author_email", "author_city",
               "title", "text", "item_rating", "review_ratings_count", "review_ratings_count_positive",
               "send_email_flag", "date_inserted", "date_lastchange", "review_ratings_ratio", "author_has_bought_item",
               "is_published", "advantages", "disadvantages", "recommended", "delete_flag", "review_source", "brand_id",
               "brand_name", "country_id"]
limit = 100
iteration = 0
USE_LIMIT = False
file = open(REVIEWS_FILE_NAME)
reader = csv.reader(file, delimiter="\t", quoting=csv.QUOTE_NONE)
for record in reader:
    if USE_LIMIT and iteration > limit:
        break
    if len(record) != 23:
        continue
    if record[1] in brands:
        brands_id = brands[record[1]][0]
        brands_name = brands[record[1]][1]
    else:
        brands_id = None
        brands_name = None
    if record[2] in buyers:
        country_id = buyers[record[2]]
    else:
        country_id = None
    record.append(brands_id)
    # TODO: insert brands lowered
    record.append(brands_name.lower())
    record.append(country_id)
    document = dict()
    for i in range(len(FIELD_NAMES)):
        if record[i] is not None and record[i] != "" and record[i].lower() != "null":
            if i == 13 or i == 14:
                record[i] = parser.parse(record[i])
            document[FIELD_NAMES[i]] = record[i]
    all_reviews.insert_one(document=document)
    iteration += 1
    if iteration%100 == 0:
        print("iteration "+str(iteration))
print("finished")

