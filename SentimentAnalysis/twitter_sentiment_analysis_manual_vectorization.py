from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.linalg import SparseVector
from pyspark.mllib.classification import LogisticRegressionWithSGD
from pyspark import SparkConf
from pyspark.context import SparkContext
conf = SparkConf().setAppName('sentiment analysis model training').setMaster('yarn-client').set("spark.executor.memory", "6g")
if 'sc' not in globals():
    sc = SparkContext(conf=conf)

FILE_NAME = 'alldata.tsv'
print "file name  is "+FILE_NAME
file = open(FILE_NAME)
import csv
reader = csv.reader(file)
words_dict = dict()
word_id = 0
lines = 0
for line in reader:
    # if lines > 1000: break
    if len(line) == 2:
        lines += 1
        for word in line[1].split():
            if len(word) < 2:
                continue
            if word not in words_dict:
                words_dict[word] = word_id
                word_id += 1
print "!!!!!!!!!!!!!      all features extracted"
del file
del reader
file = open(FILE_NAME)
import csv
all_data = []
reader = csv.reader(file)
lines = 0
for line in reader:
    # if lines > 1000: break
    if len(line) == 2:
        lines += 1
        feat = dict()
        for word in line[1].split():
            if len(word) < 2:
                continue
            if word in words_dict:
                feat[words_dict[word]] = 1.0
        feat = SparseVector(len(words_dict), feat)
        feat = LabeledPoint(line[0], feat)
        all_data.append(feat)
print "!!!!!!!!!!!!!      all features parallelized"
all_data = sc.parallelize(list(all_data))
print "!!!!!!!!!!!!!      all data parallelized"
del file
del reader
train_data, test_data = all_data.randomSplit([0.95, 0.05])
print "!!!!!!!!!!!!!      all data splitted"
clf = LogisticRegressionWithSGD.train(train_data)
print "!!!!!!!!!!!!!      all data trained"
test_result = test_data.map(lambda test_case: (test_case.label, clf.predict(test_case.features)))
print "!!!!!!!!!!!!!      all data tested"
test_result = test_result.filter(lambda x: x[0] == x[1])
print "!!!!!!!!!!!!!      all data filtered"
accuracy = test_result.count() / float(test_data.count())
print "accuracy is :"+accuracy
