from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.linalg import SparseVector
from pyspark.mllib.classification import SVMWithSGD, SVMModel
from pyspark import SparkConf
from pyspark.context import SparkContext
import csv
import pickle
conf = SparkConf().setAppName('reviews moderation').setMaster('yarn-client').set("spark.executor.memory", "3500m")
if 'sc' not in globals():
    sc = SparkContext(conf=conf)

FILE_NAME = 'reviews2.tsv'
MODEL_OUTPUT = '/user/aabukhalil/reviews_moderation_model_mnnb.model'
DICT_OUTPUT = '/home/aabukhalil/words_dict.dict'
from nltk.stem import ISRIStemmer, PorterStemmer
import re
def stemWord(word):
    en_pattern  = re.compile("[a-zA-Z]+")
    ar_pattern  = re.compile("[\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]+")
    en_word = en_pattern.search(word)
    ar_word = ar_pattern.search(word)
    if en_word and ar_word:
        return word
    if en_word:
        try:
            en_stemmer = PorterStemmer()
            return en_stemmer.stem(word)
        except:
            return word
    if ar_word:
        try:
            ar_stemmer = ISRIStemmer()
            return ar_stemmer.stem(word)
        except:
            return word
    return word

def stemSentence(sentence):
    ret = ""
    for word in sentence.split():
        ret += stemWord(word)+" "
    return ret

import re
email_pattern = re.compile(".+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)")
mobilenumber_pattern = re.compile(".*([+0-9]{1,3})*([0-9]{9,11}).*")
username_pattern = re.compile(".*[a-zA-Z0-9_-]*[_\-@]+[a-zA-Z0-9_-]{2,}.*")
url_pattern = re.compile(".*(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-])*\/?.*")

def contains_email(sentence):
    return email_pattern.findall(sentence)


def contains_mobilenumber(sentence):
    return mobilenumber_pattern.findall(sentence)


def contains_username(sentence):
    return username_pattern.findall(sentence)


def contains_url(sentence):
    return url_pattern.findall(sentence)


def add_manual_features(sentence, rating=None, recommended=None):
    ret = sentence+"\n"
    if contains_url(sentence):
        ret += "containsurl "
    if contains_username(sentence):
        ret += "containsusername "
    if contains_mobilenumber(sentence):
        ret += "containsmobilenumber "
    if contains_email(sentence):
        ret += "containsemail "
    if rating is not None and rating not in ('0', '90'):
        ret += "rating"+str(rating)+" "
    if recommended is not None:
        ret += "recommended"+recommended+"recommended"
    return ret

def remove_n_sequent_chars(sentence, n=2):
    if len(sentence) < n:
        return sentence
    sentences = []
    for i in range(1, n+1):
        addition = ""
        for j in range(i):
            addition += " "
        sentences.append(addition+sentence)
    ret = ""
    for char_idx in range(len(sentence)):
        add = False
        for added in sentences:
            if added[char_idx] != sentence[char_idx]:
                add = True
        if add:
            ret += sentence[char_idx]
    return ret



file = open(FILE_NAME, "r")
all_data = []

x = 0
for record in csv.reader(file, delimiter="\t", quoting=csv.QUOTE_NONE):
    if len(record) != 23:
        continue
    #               title       text       adv          disadv     rating     is_pub
    text = record[7]+"\n"+record[8]+"\n"+record[18]+"\n"+record[19]
    text = remove_n_sequent_chars(text)
    try :
        text = stemSentence(text)
    except:
        pass
    rating = record[9]
    recommended = record[20]
    text = add_manual_features(text, rating, recommended)
    all_data.append((text, record[17]))
    x += 1
    # if x == 1000:
    #     break

print "file name  is "+FILE_NAME
words_dict = dict()
word_id = 0
lines = 0
for line in all_data:
    if len(line) == 2:
        lines += 1
        for word in line[0].split():
            if len(word) < 2 or len(word) > 10:
                continue
            if word not in words_dict:
                words_dict[word] = word_id
                word_id += 1


def extract_features(splitted_line):
    feat = dict()
    for word in splitted_line[0].split():
        if len(word) < 2 or len(word) > 10:
            continue
        if word in words_dict:
            feat[words_dict[word]] = 1.0
    feat = SparseVector(len(words_dict), feat)
    feat = LabeledPoint(splitted_line[1], feat)
    return feat

for i in range(len(all_data)):
    all_data[i] = extract_features(all_data[i])

all_data = sc.parallelize(list(all_data))
print "!!!!!!!!!!!!!      all features added"
train_data, test_data = all_data.randomSplit([0.95, 0.05])
print "!!!!!!!!!!!!!      all data splitted"
clf = SVMWithSGD.train(train_data)
print "!!!!!!!!!!!!!      all data trained"
test_result = test_data.map(lambda test_case: (test_case.label, clf.predict(test_case.features)))
print "!!!!!!!!!!!!!      all data tested"
test_result = test_result.filter(lambda x: x[0] == x[1])
print "!!!!!!!!!!!!!      all data filtered"
accuracy = test_result.count() / float(test_data.count())
print "!!!!!!!!!!!!!      accuracy is :"+str(accuracy)
clf.save(sc, MODEL_OUTPUT)
print "!!!!!!!!!!!!!      model saved"