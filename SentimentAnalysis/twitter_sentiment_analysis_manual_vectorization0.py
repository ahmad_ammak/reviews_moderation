from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.linalg import SparseVector
from pyspark.mllib.classification import SVMWithSGD, SVMModel
from pyspark import SparkConf
from pyspark.context import SparkContext
import pickle
conf = SparkConf().setAppName('sentiment analysis model training').setMaster('yarn-client').set("spark.executor.memory", "3500m")
if 'sc' not in globals():
    sc = SparkContext(conf=conf)

FILE_NAME = 'alldata.tsv'
MODEL_OUTPUT = '/user/aabukhalil/sent_model_mnnb.model'
DICT_OUTPUT = '/home/aabukhalil/words_dict.dict'
SAVE_NOT_LOAD = False
if SAVE_NOT_LOAD:
    print "file name  is "+FILE_NAME
    file = open(FILE_NAME)
    import csv
    reader = csv.reader(file)
    words_dict = dict()
    word_id = 0
    lines = 0
    for line in reader:
        if len(line) == 2:
            lines += 1
            for word in line[1].split():
                if len(word) < 2 or len(word) > 10:
                    continue
                if word not in words_dict:
                    words_dict[word] = word_id
                    word_id += 1
    del file
    del reader
    pickle.dump(words_dict, open(DICT_OUTPUT, "w"))
else:
    words_dict = pickle.load(open(DICT_OUTPUT))
    print "!!!!!!!!!!!!!      all features loaded"


def extract_features(splitted_line):
    feat = dict()
    for word in splitted_line[1]:
        if len(word) < 2 or len(word) > 10:
            continue
        if word in words_dict:
            feat[words_dict[word]] = 1.0
    feat = SparseVector(len(words_dict), feat)
    feat = LabeledPoint(splitted_line[0], feat)
    return feat

def get_features(splitted_line):
    feat = dict()
    for word in splitted_line[1]:
        if len(word) < 2 or len(word) > 10:
            continue
        if word in words_dict:
            feat[words_dict[word]] = 1.0
    feat = SparseVector(len(words_dict), feat)
    return feat

if SAVE_NOT_LOAD:
    print "!!!!!!!!!!!!!      all features extracted"
    FILE_NAME = '/user/aabukhalil/alldata.tsv'
    file = sc.textFile(FILE_NAME)
    all_data = file.map(lambda line: line.split(",")).map(lambda splitted_line: (float(splitted_line[0].strip('"')), splitted_line[1].strip('"')))
    all_data = all_data.filter(lambda splitted_line: len(splitted_line) == 2)
    all_data = all_data.filter(lambda splitted_line:  splitted_line[0]==0.0 or  splitted_line[0]==1.0)
    all_data = all_data.map(lambda splitted_line: (splitted_line[0], (splitted_line[1].split())))
    all_data = all_data.map(lambda splitted_line: extract_features(splitted_line))
    print "!!!!!!!!!!!!!      all features added"
    train_data, test_data = all_data.randomSplit([0.95, 0.05])
    print "!!!!!!!!!!!!!      all data splitted"
    clf = SVMWithSGD.train(train_data)
    print "!!!!!!!!!!!!!      all data trained"
    test_result = test_data.map(lambda test_case: (test_case.label, clf.predict(test_case.features)))
    print "!!!!!!!!!!!!!      all data tested"
    test_result = test_result.filter(lambda x: x[0] == x[1])
    print "!!!!!!!!!!!!!      all data filtered"
    accuracy = test_result.count() / float(test_data.count())
    print "!!!!!!!!!!!!!      accuracy is :"+str(accuracy)
    clf.save(sc, MODEL_OUTPUT)
    print "!!!!!!!!!!!!!      model saved"
else:
    clf = SVMModel.load(sc, MODEL_OUTPUT)
    print "!!!!!!!!!!!!!      model loaded"
    print type(clf)
