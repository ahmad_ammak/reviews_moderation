__author__ = 'ahmad'
from pyspark.mllib.feature import Word2Vec
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import LogisticRegressionWithSGD

FILE_NAME = '/user/aabukhalil/alldata.tsv'
file = sc.textFile(FILE_NAME)
file = file.map(lambda line: line.split(',', 1))
file = file.map(lambda line: [line[0].strip('"'), line[1].strip('"')])
corpus = file.map(lambda line: line[1])
corpus = corpus.map(lambda tweet: tweet.split())
word2vec = Word2Vec()
word2vecmodel = word2vec.fit(corpus)
all_data = file.map(lambda line: LabeledPoint(line[0], word2vecmodel.transform(line[1].split())))
train_data, test_data = all_data.randomSplit([0.95, 0.05])
clf = LogisticRegressionWithSGD.train(train_data)
