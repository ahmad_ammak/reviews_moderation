__author__ = 'aabukhalil'
from configparser import ConfigParser
from flask import Flask
import requests
import traceback
import os
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
config = ConfigParser()
config.read(currentdir+"/configs/master_configuration.ini")
app = Flask(__name__)

@app.route('/updateall')
def updateall():
    global config
    success = []
    result = ""
    for i in range(config.getint("RELIABILITY", "num_of_instances")):
        try:
            ip = config.get("RELIABILITY", str(i)+"_ip")
            port = config.get("RELIABILITY", str(i)+"_port")
            res = requests.get("http://"+ip+":"+port+"/update_moderation_model")
            if res.text == "updated":
                success.append(True)
                result += ip+":"+port+" = "+"success\n"
            else:
                success.append(False)
                result += ip+":"+port+" = "+"failed\n"
        except:
            result += ip+":"+port+" = "+"failed\n"+traceback.format_exc()
    return result

app.run(host=config.get("FLASK", "host"), port=config.getint("FLASK", "port"), debug=False)