__author__ = 'aabukhalil'
from configparser import ConfigParser

configs = ConfigParser()
configs.read("configs/config.ini")
from Classifiers.review_classifier import ReviewClassifier
r= ReviewClassifier(configs)
r.train_new_classifier_and_transformer(save_to_redis=False)
