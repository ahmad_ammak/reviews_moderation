__author__ = 'aabukhalil'
import os
import sys
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from RESTfulFlaskApi.bootstrap import *
from RESTfulFlaskApi.Models.reviews_model import ReviewsModel
from RESTfulFlaskApi.Models.stats_model import StatisticsModel
from RESTfulFlaskApi.Models.review_moderation_model import ReviewModerator
from RESTfulFlaskApi.Models.moderation_config_model import ModerationConfig
from flask import Flask, Response, request
from flask.ext.cors import cross_origin
from dateutil import parser
import json

logger.warn("Loading models started")
print("Loading models started")
reviews_model = ReviewsModel(config=configs)
statistics_model = StatisticsModel(config=configs)
review_moderator = ReviewModerator(config=configs)
moderation_config = ModerationConfig(config=configs)
logger.warn("Loading models finished")
print("Loading models finished")

app = Flask(__name__)
app.logger.addHandler(handler)


def get_reviews_request_arguments(request_arguments):
    parameters = ('brand_name', 'id_item', 'country_id', 'brand_id', 'id_item_review', 'is_published', 'item_rating',
                  'after_date', 'before_date')
    arguments = dict()
    if request_arguments is None:
        return arguments
    for parameter in parameters:
        argument = request_arguments.args.get(parameter)
        if argument is not None and argument.strip() != "":
            if parameter.find("date") != -1:
                try:
                    argument = parser.parse(argument)
                    arguments[parameter] = argument
                except Exception as e:
                    logger.exception(e)
            else:
                arguments[parameter] = argument
    return arguments


def get_moderate_request_arguments(request_arguments):
    mandatory_parameters = ('title', 'text', 'item_rating', 'advantages', 'disadvantages', 'recommended')
    optional_parameters = ('author_name', 'author_email', 'author_city', 'review_ratings_count',
                           'review_ratings_count_positive', 'review_ratings_ratio', 'author_has_bought_item',
                           'id_item_review', 'id_item', 'id_author', 'id_language')
    arguments = dict()
    if request_arguments is None:
        return None, "one or more of these is None or empty " + mandatory_parameters.__repr__() \
               + "\n These are mandetory arguments"
    for parameter in mandatory_parameters:
        argument = request_arguments.args.get(parameter)
        if argument is not None and argument.strip() != "":
            arguments[parameter] = argument
        else:
            arguments[parameter] = ""
    for parameter in optional_parameters:
        argument = request_arguments.args.get(parameter)
        if argument is not None and argument.strip() != "":
            arguments[parameter] = argument
        else:
            arguments[parameter] = ""
    return True, arguments


def get_moderation_configs_arguments(request_arguments):
    parameters = [i for i in moderation_config.get_all_parameters()]
    arguments = dict()
    if request_arguments is None:
        return arguments
    for parameter in parameters:
        argument = request_arguments.args.get(parameter)
        if argument is not None and argument.strip() != "":
            arguments[parameter] = argument
    return arguments


@app.route('/get_reviews_where', methods=['GET', 'POST'])
@cross_origin(origin='*')
def get_reviews_where():
    arguments = get_reviews_request_arguments(request)
    result = [review for review in reviews_model.get_string_reviews_where(arguments)]
    result = json.dumps(result)
    return Response(response=result, status=200, mimetype="application/json")


@app.route('/get_stats_for', methods=['GET', 'POST'])
@cross_origin(origin='*')
def get_stats_for():
    arguments = get_reviews_request_arguments(request)
    result = statistics_model.get_statistics_for(arguments)
    result = json.dumps(result)
    return Response(response=result, status=200, mimetype="application/json")


@app.route('/update_moderation_model', methods=['GET', 'POST'])
@cross_origin(origin='*')
def update():
    result = review_moderator.update()
    result = json.dumps(result)
    return Response(response=result, status=200, mimetype="application/json")


@app.route('/moderate_review', methods=['GET', 'POST'])
@cross_origin(origin='*')
def moderate_review():
    review = get_moderate_request_arguments(request)
    if review[0] is None:
        result = review[1]
    else:
        result = review_moderator.moderate_review(review=review[1])
    result = json.dumps(result)
    return Response(response=result, status=200, mimetype="application/json")


@app.route('/get_all_parameters', methods=['GET', 'POST'])
@cross_origin(origin='*')
def get_all_parameters():
    result = moderation_config.get_all_parameters()
    result = json.dumps(result)
    return Response(response=result, status=200, mimetype="application/json")


@app.route('/set_parameters', methods=['GET', 'POST'])
@cross_origin(origin='*')
def set_parameters():
    arguments = get_moderation_configs_arguments(request)
    result = moderation_config.set_parameters(arguments)
    result = json.dumps(result)
    return Response(response=result, status=200, mimetype="application/json")


@app.route('/is_deserve_update', methods=['GET', 'POST'])
@cross_origin(origin='*')
def is_deserve_update():
    result = review_moderator.is_deserve_update()
    result = json.dumps(result)
    return Response(response=result, status=200, mimetype="application/json")


if __name__ == "__main__":
    logger.warn("App started")
    print("App started")
    app.run(
        host=configs.get("FLASK", "host"), port=configs.getint("FLASK", "port"),
        debug=configs.getboolean("FLASK", "debug"), threaded=configs.getboolean("FLASK", "threaded")
    )
    logger.warn("App stopped")
    print("App stopped")
