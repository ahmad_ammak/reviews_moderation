__author__ = 'aabukhalil'
from ming import create_datastore
from ming.odm import ThreadLocalODMSession
from ming import schema
from ming.odm import FieldProperty, Mapper
from ming.odm.declarative import MappedClass

session = ThreadLocalODMSession(bind=create_datastore('mongodb://localhost:27017/reviews'))


class Review(MappedClass):
    def __init__(self, session):
        super().__init__()
        self.session = session

    class __mongometa__:
        session = session
        name = 'all_reviews'

    _id = FieldProperty(schema.ObjectId)
    id_item_review = FieldProperty(schema.String)
    item_rating = FieldProperty(schema.String)
    author_has_bought_item = FieldProperty(schema.String)
    is_published = FieldProperty(schema.String)
    text = FieldProperty(schema.String)
    id_language = FieldProperty(schema.String)
    send_email_flag = FieldProperty(schema.String)
    delete_flag = FieldProperty(schema.String)
    review_ratings_ratio = FieldProperty(schema.String)
    id_item = FieldProperty(schema.String)
    date_lastchange = FieldProperty(schema.DateTime)
    title = FieldProperty(schema.String)
    date_inserted = FieldProperty(schema.DateTime)
    brand_name = FieldProperty(schema.String)
    brand_id = FieldProperty(schema.String)
    review_ratings_count_positive = FieldProperty(schema.String)
    id_author = FieldProperty(schema.String)
    country_id = FieldProperty(schema.String)
    review_ratings_count = FieldProperty(schema.String)




Mapper.compile_all()
reviews = Review.query.find({"brand_name": "google"})
for review in reviews:
    print(type(review))