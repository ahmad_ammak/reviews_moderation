__author__ = 'aabukhalil'
import logging
from log4mongo.handlers import MongoHandler
from configparser import ConfigParser
import os
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

configs = ConfigParser()
configs.read(currentdir+"/configs/config.ini")
handler = MongoHandler(host=configs.get("LOGGING", "mongo_host"), port=configs.getint("LOGGING", "mongo_port"),
                       database_name=configs.get("LOGGING", "mongo_database"),
                       collection=configs.get("LOGGING", "mongo_collection"))
logger = logging.getLogger("main_logger")
logger.addHandler(handler)