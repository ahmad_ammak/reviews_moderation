__author__ = 'aabukhalil'
from configparser import ConfigParser


class ModerationConfig:
    def __init__(self, config):
        super().__init__()
        self.config = config

    def get_all_parameters(self):
        config = ConfigParser()
        import os
        import inspect
        currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        currentdir = os.path.dirname(currentdir)
        moderation_config_file = currentdir+"/"+self.config.get("MODERATION", "moderation_config_file_path")
        config.read(moderation_config_file)
        moderation_configs = dict([i for i in config.items("MODERATION")])
        return moderation_configs

    def set_parameters(self, arguments):
        config = ConfigParser()
        import os
        import inspect
        currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        currentdir = os.path.dirname(currentdir)
        moderation_config_file = currentdir+"/"+self.config.get("MODERATION", "moderation_config_file_path")
        config.read(moderation_config_file)
        for argument in arguments:
                config.set("MODERATION", argument, arguments[argument])
        config.write(open(moderation_config_file, "w"))
        return "configs saved successfully"
