__author__ = 'aabukhalil'
from RESTfulFlaskApi.Models.reviews_model import ReviewsModel
from collections import defaultdict


class StatisticsModel:
    def __init__(self, config):
        super().__init__()
        self.config = config
        StatisticsModel.reviews_model = ReviewsModel(config)

    def get_statistics_for(self, conditions):
        total_reviews_number = 0
        total_accepted_reviews = 0
        authors = set()
        distinct_items = defaultdict(lambda: 0)
        languages = defaultdict(lambda: 0)
        ratings = defaultdict(lambda: 0)
        brands = defaultdict(lambda: 0)
        countries = defaultdict(lambda: 0)
        newest_review = None
        oldest_review = None
        for review in StatisticsModel.reviews_model.get_reviews_where(conditions):
            distinct_items[review["id_item"]] += 1
            languages[review["id_language"]] += 1
            total_accepted_reviews += 1
            ratings[review["item_rating"]] += 1
            if "brand_name" in review:
                brands[review["brand_name"]] += 1
            else:
                brands["Not Specified"] += 1
            if "country_id" in review:
                countries[review["country_id"]] += 1
            else:
                countries["Not Specified"] += 1
            if newest_review is None:
                newest_review = review["date_inserted"]
            else:
                if newest_review < review["date_inserted"]:
                    newest_review = review["date_inserted"]
            if oldest_review is None:
                oldest_review = review["date_inserted"]
            else:
                if oldest_review > review["date_inserted"]:
                    oldest_review = review["date_inserted"]
            authors.add(review["id_author"])
            total_reviews_number += 1

        results = dict()
        results["total_reviews_number"] = total_reviews_number
        results["total_accepted_reviews"] = total_accepted_reviews
        results["item_ids_and_count"] = distinct_items
        results["number_of_authors"] = str(len(authors))
        results["languages"] = languages
        results["ratings"] = ratings
        if newest_review is not None:
            results["newest_review"] = newest_review.strftime("%Y-%m-%d %H:%M:%S")
        if oldest_review is not None:
            results["oldest_review"] = oldest_review.strftime("%Y-%m-%d %H:%M:%S")
        results["brands"] = brands
        results["countries"] = countries
        return results
