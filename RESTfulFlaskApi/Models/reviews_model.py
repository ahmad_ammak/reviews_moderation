__author__ = 'aabukhalil'
from pymongo import MongoClient
from datetime import datetime


class ReviewsModel:
    def __init__(self, config):
        super().__init__()
        client = MongoClient(config.get("MONGODB", "host"), config.getint("MONGODB", "port"))
        db = client.get_database(config.get("MONGODB", "database"))
        ReviewsModel.collection_name = config.get("MONGODB", "collection_name")
        ReviewsModel.collection = db.get_collection(ReviewsModel.collection_name)
        self.config = config
        self.max_return_limit = config.getint("MONGODB", "max_return_limit")

    def get_reviews_where(self, conditions):
        mongo_conditions = dict()
        for key in conditions:
            if key.find("date") == -1:
                mongo_conditions[key] = conditions[key]
            else:
                if key == "after_date":
                    mongo_conditions["date_inserted"] = {"$gte": conditions["after_date"]}
                elif key == "before_date":
                    if "date_inserted" in mongo_conditions:
                        mongo_conditions["$and"] = [
                            {"date_inserted": {"$gte": conditions["after_date"]}},
                            {"date_inserted": {"$lte": conditions["before_date"]}}
                        ]
                        mongo_conditions.pop("date_inserted")
                    else:
                        mongo_conditions["date_inserted"] = {"$lte": conditions["before_date"]}
        hidden_fields = dict()
        hidden_fields["_id"] = 0
        for review in ReviewsModel.collection.find(mongo_conditions, hidden_fields).limit(self.max_return_limit):
            yield review

    def get_string_reviews_where(self, conditions):
        for review in self.get_reviews_where(conditions):
            string_review = dict()
            for key in review:
                if isinstance(review[key], datetime):
                    string_review[key] = review[key].strftime("%Y-%m-%d %H:%M:%S")
                else:
                    string_review[key] = review[key]
            yield string_review
