__author__ = 'aabukhalil'
from Classifiers.review_classifier import ReviewClassifier
from configparser import ConfigParser

class ReviewModerator:
    def __init__(self, config):
        super().__init__()
        self.config = config
        self.moderation_config = ConfigParser()
        import os
        import inspect
        currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        currentdir = os.path.dirname(currentdir)
        self.moderation_config.read(currentdir+"/"+self.config.get("MODERATION", "moderation_config_file_path"))
        self.review_classifier = ReviewClassifier(config)
        self.review_classifier.load_classifier_and_transformer()

    def update(self):
        if self.review_classifier.update_redis_models_with_new_data():
            return "updated"
        else:
            return "failed"

    def update_configs(self):
        import os
        import inspect
        currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        currentdir = os.path.dirname(currentdir)
        self.moderation_config.read(currentdir+"/"+self.config.get("MODERATION", "moderation_config_file_path"))

    def moderate_review(self, review):
        result = self.review_classifier.classify(review,
                                                 accept_threshold=self.moderation_config.getfloat("MODERATION",
                                                                                                  "accept_threshold"),
                                                 reject_threshold=self.moderation_config.getfloat("MODERATION",
                                                                                                  "reject_threshold")
                                                 )
        ret = dict()
        if not result:
            ret["moderated"] = False
        else:
            ret["moderated"] = True
            ret["result"] = result[0]
            ret["confidence"] = result[1]
        return ret

    def is_deserve_update(self):
        threshold = self.moderation_config.getint("MODERATION", "partial_fit_batch_size")
        return self.review_classifier.is_there_is_new_reviews_more_than_threshold(threshold)